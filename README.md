# C Scrapbook #

This repo contains scrapbook for various tests and examples in good ol' C.

### Examples ###

* fp.c - example of closures implementation in C, using functional pointers and structures. Basic functions, like flip and add are provided.

### Compilation ###

I'm trying to not use anything compiler-specific, so most of the code can be compiled by any modern C compiler.

Example compile commandline:
```
#!bash
gcc fp.c -o fp
```

### Who do I talk to? ###

I hope you'll find this useful. In case of any questions please do not hesitate to contact me: Alexander Samolov (alexsamolov@gmail.com)