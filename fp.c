#include <stdio.h>

struct closure {
    int (*f)(int, int);
    int a;
    int b;
};

struct closure flip(struct closure a) {
    struct closure c = {0};
    c.f = a.f;
    c.a = a.b;
    c.b = a.a;
    return c;
}

int add(int a, int b) {
    int c = a + b;
    printf(" %d + %d = %d\n", a, b, c);
    return c;
}

int run(struct closure c) {
    return c.f(c.a, c.b);
}

int main(int argc, char** argv) {
    struct closure c = {0};
    c.f = add;
    c.a = 5;
    c.b = 6;
    printf("Regular closure:");
    run(c);
    printf("Flipped closure:");
    run(flip(c));
    return 0;
}
